package com.ps.bankdesawlaharwetan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ExpandedMenuView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class SetoranTunai extends AppCompatActivity
{
    EditText TBSetoran;
    ProgressDialog pDialog;
    private static final int REQUEST_ENABLE_BT = 2;
    BluetoothService mService = null;
    BluetoothDevice con_dev = null;
    private static final int REQUEST_CONNECT_DEVICE = 1;  //获取设备消息

    String nama, alamat, jenisrekening, norekening;

    private void printImage() {
        byte[] sendData = null;
        PrintPic pg = new PrintPic();
        pg.initCanvas(384);
        pg.initPaint();
        pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
        sendData = pg.printDraw();
        mService.write(sendData);   //打印byte流数据
    }

    private final  Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:   //已连接
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            String formattedDate = df.format(c.getTime());
                            String seconds = String.valueOf(c.get(Calendar.MILLISECOND));
                            Random rand = new Random();
                            int n = rand.nextInt(9999);

                            Toast.makeText(getApplicationContext(), "Connect successful",
                                    Toast.LENGTH_SHORT).show();

                            String isi = "";
                            byte[] cmd = new byte[3];
                            cmd[0] = 0x1b;
                            cmd[1] = 0x21;
                            cmd[2] |= 0x10;
                            mService.write(cmd);
                            cmd[2] &= 0xEF;
                            mService.write(cmd);           //取消倍高、倍宽模式
                            isi =   "            BANK DESA\n" +
                                    "           WLAHAR WETAN\n\n" +
                                    formattedDate + "                   " + seconds +
                                    "\n" +
                                    "           SLIP SETORAN         " +
                                    "Kode: " + String.valueOf(n) + "\n" +
                                    "--------------------------------" +
                                    "Nama: "+nama+"\n" +
                                    "Jenis Rekening: "+jenisrekening+"\n" +
                                    "No. Rekening: " + norekening +
                                    "\n--------------------------------" +
                                    "Setor Tunai Sebesar:\n" +
                                    "Rp. " + TBSetoran.getText().toString() +
                                    ",00\ntelah berhasil\n\n\n" +
                                    "           TERIMA KASIH\n\n" +
                                    "========== INFO LENGKAP ========" +
                                    "          WWW.BDWW.CO.ID\n\n\n";


                            mService.sendMessage(isi,"GBK");

                            break;
                        case BluetoothService.STATE_CONNECTING:  //正在连接
                            Log.d("蓝牙调试","正在连接.....");
                            break;
                        case BluetoothService.STATE_LISTEN:     //监听连接的到来
                        case BluetoothService.STATE_NONE:
                            Log.d("蓝牙调试","等待连接.....");
                            break;
                    }
                    break;
                case BluetoothService.MESSAGE_CONNECTION_LOST:    //蓝牙已断开连接
                    Toast.makeText(getApplicationContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothService.MESSAGE_UNABLE_CONNECT:     //无法连接设备
                    Toast.makeText(getApplicationContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setoran_tunai);

        mService = new BluetoothService(this, mHandler);

        Intent intent = getIntent();
        nama = intent.getStringExtra("nama");
        alamat = intent.getStringExtra("alamat");
        jenisrekening = intent.getStringExtra("jenisrekening");
        norekening = intent.getStringExtra("norekening");

        TBSetoran = (EditText) findViewById(R.id.TBSetoran);
        Button BProses = (Button) findViewById(R.id.BProses);
        final Button BCetak = (Button) findViewById(R.id.BCetak);

        BProses.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                pDialog = new ProgressDialog(SetoranTunai.this);
                pDialog.setMessage("Mengecek...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        pDialog.dismiss();
                        BCetak.setVisibility(View.VISIBLE);
                    }
                }, 1000);
            }
        });

        BCetak.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent serverIntent = new Intent(SetoranTunai.this, DeviceListActivity.class);      //运行另外一个类的活动
                startActivityForResult(serverIntent,REQUEST_CONNECT_DEVICE);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //蓝牙未打开，打开蓝牙
        if( mService.isBTopen() == false)
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        try {

        } catch (Exception ex) {
            Log.e("出错信息",ex.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null)
            mService.stop();
        mService = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:      //请求打开蓝牙
                if (resultCode == Activity.RESULT_OK) {   //蓝牙已经打开
                    Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
                } else {                 //用户不允许打开蓝牙
                    finish();
                }
                break;
            case  REQUEST_CONNECT_DEVICE:     //请求连接某一蓝牙设备
                if (resultCode == Activity.RESULT_OK) {   //已点击搜索列表中的某个设备项
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //获取列表项中设备的mac地址
                    con_dev = mService.getDevByMac(address);

                    mService.connect(con_dev);
                }
                break;
        }
    }
}
