package com.ps.bankdesawlaharwetan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ps.bankdesawlaharwetan.App.RequestHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ActivitySetorTunai extends AppCompatActivity
{
    EditText TBNoRekening;
    TextView LNama, LAlamat, LJenisKelamin, LTanggalLahir, LJenisRekening;
    Button BCekRekening, BSetorTunai;
    LinearLayout LLRekening;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setor_tunai);

        TBNoRekening = (EditText) findViewById(R.id.TBNoRekening);
        LNama = (TextView) findViewById(R.id.LNama);
        LAlamat = (TextView) findViewById(R.id.LAlamat);
        LJenisKelamin = (TextView) findViewById(R.id.LJenisKelamin);
        LTanggalLahir = (TextView) findViewById(R.id.LTanggalLahir);
        LJenisRekening = (TextView) findViewById(R.id.LJenisRekening);
        BCekRekening = (Button) findViewById(R.id.BCekRekening);
        BSetorTunai = (Button) findViewById(R.id.BSetorTunai);
        LLRekening = (LinearLayout) findViewById(R.id.LLRekening);

        BCekRekening.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new CekRekeningManager(getApplicationContext()).execute(TBNoRekening.getText().toString());
            }
        });

        BSetorTunai.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), SetoranTunai.class);
                intent.putExtra("nama", LNama.getText());
                intent.putExtra("alamat", LAlamat.getText());
                intent.putExtra("jenisrekening", LJenisRekening.getText());
                intent.putExtra("norekening", TBNoRekening.getText().toString());
                startActivity(intent);
            }
        });
    }

    class CekRekeningManager extends AsyncTask<String, Void, String>
    {
        private ProgressDialog pDialog;
        private Context context;

        public CekRekeningManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActivitySetorTunai.this);
            pDialog.setMessage("Mengecek...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("norekening", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_ambil_rekening);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    LNama.setText(jsonObj.getString("nama"));
                    LAlamat.setText(jsonObj.getString("alamat"));
                    LJenisKelamin.setText(jsonObj.getString("jeniskelamin") == "0" ? "Laki-Laki" : "Perempuan");
                    LTanggalLahir.setText(jsonObj.getString("tanggallahir"));
                    LJenisRekening.setText(jsonObj.getString("jenisrekening") == "0" ? "Percobaan" : "Tabungan Ku");
                    LLRekening.setVisibility(View.VISIBLE);
                } catch (JSONException e)
                {
                }
            } else
            {

            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
